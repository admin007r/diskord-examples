This is a Gradle platform subproject to help manage dependency versions.

See https://docs.gradle.org/current/userguide/java_platform_plugin.html for more information.
